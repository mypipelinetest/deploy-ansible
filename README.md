
# récupération des roles
ansible-galaxy install --roles-path roles/ -r requirements.yml

# déploiements des environnements
ansible-playbook -i env/dev/hosts -u vagrant -k install-myapp1.yml
ansible-playbook -i env/prod/hosts -u vagrant -k install-myapp1.yml